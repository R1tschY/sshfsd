#include <argp.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <vector>

#include <QRunnable>
#include <libssh/callbacks.h>
#include <libssh/libssh.h>
#include <libssh/server.h>
#include <libssh/sftp.h>

#include <sftp_commands.h>


#define KEYS_FOLDER "/home/richard/tmp/"

#define USER "myuser"
#define PASSWORD "mypassword"

class SshFsThread : public QRunnable {
public:
    SshFsThread() {

    }

    void run() override;
};

void SshFsThread::run()
{

}

class SshServer {
public:
    SshServer(const QString& user, const QString& password, int passwordTries)
    : m_user(user)
    , m_password(password)
    , m_passwordTries(passwordTries)
    {}

    int passwordTries() const {
        return m_passwordTries;
    }

    bool checkPassword(const char* user, const char* password) const {
        return strcmp(user, USER) == 0 && strcmp(password, PASSWORD) == 0;
    }

private:
    QString m_user;
    QString m_password;
    int m_passwordTries;
};


class SshServerSession {
public:
    SshServerSession(ssh_session session, SshServer* server);

    ~SshServerSession() {
        if (m_session) {
            ssh_disconnect(m_session.get());
        }
    }

    void disconnect() {
        ssh_disconnect(m_session.get());
        m_connected = false;
    }

    bool connected() {
        return m_connected;
    }

    sftp_session sftpSession() const {
        return m_sftpSession;
    }

    int authorizeWithPassword(const char* user, const char* password);
    void startSftpSession(ssh_channel channel);
    ssh_channel createChannel();

private:
    SshSessionPtr m_session = nullptr;
    ssh_server_callbacks_struct m_cb = {};
    sftp_session m_sftpSession = nullptr;
    
    std::vector<SshChannelPtr> m_channels;
    ssh_channel_callbacks_struct m_channel_cb = {};

    SshServer* m_server = nullptr;

    bool m_connected = true;
    bool m_authenticated = false;
    int m_leftTries = 3;
};

static ssh_channel new_session_channel(ssh_session session, void* userdata)
{
    Q_UNUSED(session);
    Q_ASSERT(userdata != nullptr);
    return reinterpret_cast<SshServerSession*>(userdata)->createChannel();
}

ssh_channel SshServerSession::createChannel() {
    printf("Allocated session channel\n");
    ssh_channel channel = ssh_channel_new(m_session.get());
    m_channels.emplace_back(channel);
    ssh_set_channel_callbacks(channel, &m_channel_cb);
    return channel;
}

static int auth_password(ssh_session session, const char* user,
    const char* password, void* userdata)
{
    Q_ASSERT(userdata != nullptr);
    return reinterpret_cast<SshServerSession*>(userdata)->authorizeWithPassword(user, password);
}

int SshServerSession::authorizeWithPassword(const char* user, const char* password) {
    printf("Authenticating user %s pwd %s\n", user, password);
    if (m_server->checkPassword(user, password)) {
        m_authenticated = true;
        printf("Authenticated\n");
        return SSH_AUTH_SUCCESS;
    }
    m_leftTries -= 1;
    if (m_leftTries == 0) {
        printf("Too many authentication tries\n");
        disconnect();
        return SSH_AUTH_DENIED;
    }
    return SSH_AUTH_DENIED;
}

static int subsystem_request(
    ssh_session session,
    ssh_channel channel,
    const char* subsystem,
    void* userdata)
{
    (void)userdata;
    printf("Subsystem request %s\n", subsystem);

    if (strcmp(subsystem, "sftp") == 0) {
        reinterpret_cast<SshServerSession*>(userdata)->startSftpSession(channel);
        printf("Start SFTP\n");
        return SSH_OK;
    }

    return SSH_OK;
}

SshServerSession::SshServerSession(ssh_session session, SshServer* server)
: m_session(session)
, m_server(server)
, m_leftTries(server->passwordTries())
{
    printf("new_session_channel\n");

    m_cb.userdata = this;
    m_cb.auth_password_function = auth_password;
    m_cb.channel_open_request_session_function = new_session_channel;

    ssh_callbacks_init(&m_cb);
    ssh_set_server_callbacks(session, &m_cb);

    m_channel_cb.userdata = this;
    m_channel_cb.channel_subsystem_request_function = subsystem_request;
    ssh_callbacks_init(&m_channel_cb);

    ssh_options_set(session, SSH_OPTIONS_COMPRESSION, "yes");
}

void SshServerSession::startSftpSession(ssh_channel channel) {
    m_sftpSession = sftp_server_new(m_session.get(), channel);
}



const char* argp_program_version = "libssh server example " SSH_STRINGIFY(LIBSSH_VERSION);
const char* argp_program_bug_address = "<libssh@libssh.org>";

/* Program documentation. */
static char doc[] = "libssh -- a Secure Shell protocol implementation";

/* A description of the arguments we accept. */
static char args_doc[] = "BINDADDR";

/* The options we understand. */
static struct argp_option options[] = {
    { .name = "port",
        .key = 'p',
        .arg = "PORT",
        .flags = 0,
        .doc = "Set the port to bind",
        .group = 0 },
    { .name = "hostkey",
        .key = 'k',
        .arg = "FILE",
        .flags = 0,
        .doc = "Set the host key",
        .group = 0 },
    { .name = "dsakey",
        .key = 'd',
        .arg = "FILE",
        .flags = 0,
        .doc = "Set the dsa key",
        .group = 0 },
    { .name = "rsakey",
        .key = 'r',
        .arg = "FILE",
        .flags = 0,
        .doc = "Set the rsa key",
        .group = 0 },
    { .name = "verbose",
        .key = 'v',
        .arg = nullptr,
        .flags = 0,
        .doc = "Get verbose output",
        .group = 0 },
    { nullptr, 0, nullptr, 0, nullptr, 0 }
};

/* Parse a single option. */
static error_t parse_opt(int key, char* arg, struct argp_state* state)
{
    /* Get the input argument from argp_parse, which we
     * know is a pointer to our arguments structure.
     */
    ssh_bind sshbind = reinterpret_cast<ssh_bind>(state->input);

    switch (key) {
    case 'p':
        ssh_bind_options_set(sshbind, SSH_BIND_OPTIONS_BINDPORT_STR, arg);
        break;
    case 'd':
        ssh_bind_options_set(sshbind, SSH_BIND_OPTIONS_DSAKEY, arg);
        break;
    case 'k':
        ssh_bind_options_set(sshbind, SSH_BIND_OPTIONS_HOSTKEY, arg);
        break;
    case 'r':
        ssh_bind_options_set(sshbind, SSH_BIND_OPTIONS_RSAKEY, arg);
        break;
    case 'v':
        ssh_bind_options_set(sshbind, SSH_BIND_OPTIONS_LOG_VERBOSITY_STR, "3");
        break;
    case ARGP_KEY_ARG:
        if (state->arg_num >= 1) {
            /* Too many arguments. */
            argp_usage(state);
        }
        ssh_bind_options_set(sshbind, SSH_BIND_OPTIONS_BINDADDR, arg);
        break;
    case ARGP_KEY_END:
        if (state->arg_num < 1) {
            /* Not enough arguments. */
            argp_usage(state);
        }
        break;
    default:
        return ARGP_ERR_UNKNOWN;
    }

    return 0;
}

/* Our argp parser. */
static struct argp argp = { options, parse_opt, args_doc, doc, nullptr, nullptr, nullptr };

class SshEventQueue {
public:
    SshEventQueue() : m_eventqueue(ssh_event_new()) { }
    ~SshEventQueue() { ssh_event_free(m_eventqueue);  }

    bool run(int timeout) {
        int rc = SSH_OK;
        while (rc == SSH_OK)
        {
            printf("POLL\n");
            rc = ssh_event_dopoll(m_eventqueue, timeout);
        }
        return rc != SSH_ERROR;
    }

private:
    ssh_event m_eventqueue;
    QString m_error;
};


int main(int argc, char** argv)
{
    ssh_session session;
    ssh_bind sshbind;
    ssh_event mainloop;

    SshServer server(USER, PASSWORD, 3);

    int r;

    sshbind = ssh_bind_new();
    session = ssh_new();

    //ssh_bind_options_set(sshbind, SSH_BIND_OPTIONS_ECDSAKEY, KEYS_FOLDER "ssh_host_ecdsa_key");
    ssh_bind_options_set(sshbind, SSH_BIND_OPTIONS_RSAKEY, KEYS_FOLDER "ssh_host_rsa_key");

    /*
     * Parse our arguments; every option seen by parse_opt will
     * be reflected in arguments.
     */
    argp_parse(&argp, argc, argv, 0, nullptr, sshbind);

    if (ssh_bind_listen(sshbind) < 0) {
        printf("Error listening to socket: %s\n", ssh_get_error(sshbind));
        return 1;
    }
    r = ssh_bind_accept(sshbind, session);
    if (r == SSH_ERROR) {
        printf("error accepting a connection : %s\n", ssh_get_error(sshbind));
        return 1;
    }

    {
        SshServerSession serverSession(session, &server);

        printf("ssh_handle_key_exchange\n");
        if (ssh_handle_key_exchange(session)) {
            printf("ssh_handle_key_exchange: %s\n", ssh_get_error(session));
            return 1;
        }
        printf("ssh_set_auth_methods\n");
        ssh_set_auth_methods(session, SSH_AUTH_METHOD_PASSWORD);
        mainloop = ssh_event_new();
        printf("ssh_event_add_session\n");
        ssh_event_add_session(mainloop, session);

        while (serverSession.connected() && serverSession.sftpSession() == nullptr) {
            printf("POLL\n");
            r = ssh_event_dopoll(mainloop, -1);
            if (r == SSH_ERROR) {
                printf("Error : %s\n", ssh_get_error(session));
                ssh_disconnect(session);
                return 1;
            }
        }

        if (!serverSession.connected()) {
            printf("Error, exiting loop\n");
        }

        SftpServerSession sftpSession(serverSession.sftpSession());
        sftpSession.run();
    }

    ssh_bind_free(sshbind);
    ssh_finalize();
    return 0;
}
