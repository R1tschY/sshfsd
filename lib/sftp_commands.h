#include <QDir>
#include <QDirIterator>
#include <QString>
#include <libssh/sftp.h>
#include <memory>
#include <unordered_map>

struct SshSessionDestructor {
    void operator()(ssh_session ptr)
    {
        ssh_free(ptr);
    }
};
using SshSessionPtr = std::unique_ptr<ssh_session_struct, SshSessionDestructor>;

struct SshChannelDestructor {
    void operator()(ssh_channel ptr)
    {
        ssh_channel_free(ptr);
    }
};
using SshChannelPtr = std::unique_ptr<ssh_channel_struct, SshChannelDestructor>;

struct SftpSessionDestructor {
    void operator()(sftp_session ptr)
    {
        sftp_free(ptr);
    }
};
using SftpSessionPtr = std::unique_ptr<sftp_session_struct, SftpSessionDestructor>;

struct SshStringDestructor {
    void operator()(ssh_string ptr)
    {
        ssh_string_free(ptr);
    }
};
using SshStringPtr = std::unique_ptr<ssh_string_struct, SshStringDestructor>;

struct SshCharsDestructor {
    void operator()(char* ptr)
    {
        ssh_string_free_char(ptr);
    }
};
using SshCharPtr = std::unique_ptr<char, SshCharsDestructor>;

template<typename Func>
class ScopeGuard
{
public:
  explicit ScopeGuard(const Func& func) : func_(func) { }
  explicit ScopeGuard(Func&& func) : func_(std::move(func)) { }

  ScopeGuard(ScopeGuard&& other)
  noexcept(std::is_nothrow_move_constructible<Func>::value)
  : func_(std::move_if_noexcept(other.func_)), active_(other.active_)
  {
    other.active_ = false;
  }

  /// execute final action
  ~ScopeGuard() { if (active_) func_(); }

  // non-copyable
  ScopeGuard(const ScopeGuard&) = delete;
  ScopeGuard& operator=(const ScopeGuard&) = delete;

private:
  Func func_;
  bool active_ = true;
};

template<typename Func>
ScopeGuard<Func> finally(Func&& func)
{
  return ScopeGuard<Func>(std::forward<Func>(func));
}

class SftpServerSession {
public:
    SftpServerSession(sftp_session sftp);
    ~SftpServerSession();

    void run();

private:
    QString getFilename(sftp_client_message msg);
    sftp_attributes_struct getFileAttrs(const QFileInfo& fileInfo);

    bool replyReadLink(sftp_client_message msg);
    bool replyRealPath(sftp_client_message msg);
    bool replyStat(sftp_client_message msg, bool link);

    bool replyOpenDir(sftp_client_message msg);
    bool replyReadDir(sftp_client_message msg);
    bool replyClose(sftp_client_message msg);

    bool replyOpenFile(sftp_client_message msg);
    bool replyReadFile(sftp_client_message msg);
    bool replyCloseFile(sftp_client_message msg);

    bool replyUnsupported(sftp_client_message msg);
    bool replyOk(sftp_client_message msg);

    SftpSessionPtr m_sftpSession;
    QDir m_cwd;

    std::unordered_map<void*, std::unique_ptr<QDirIterator>> m_openDirHandles;
    std::unordered_map<void*, std::unique_ptr<QFile>> m_openFileHandles;
};
