#include <QtTest/QtTest>

#include <QCommandLineParser>
#include <QProcess>
#include <QTemporaryDir>
#include <libssh/libssh.h>

#include "sftp_commands.h"

bool generateRsaKey(const QString& filePath);

#define getSftpErrorString(sftpSession) (strerror(sftp_get_error(sftpSession)))
#define toChars(x) (x.toUtf8().constData())


class TestSftpServer : public QObject {
    Q_OBJECT
private slots:

    void initTestCase();
    void cleanupTestCase();

    void testSftpConnection()
    {
        QCOMPARE(sftp_server_version(m_sftpSession.get()), 3);
    }

    void testStartPwd()
    {
        SshCharPtr result(sftp_canonicalize_path(m_sftpSession.get(), "."));
        QCOMPARE(result.get(), "/home");
    }

    //
    // REALPATH

    void testRealpathDotDot()
    {
        SshCharPtr result(sftp_canonicalize_path(m_sftpSession.get(), "/home/.."));
        QVERIFY2(result != nullptr, getSftpErrorString(m_sftpSession.get()));
        QCOMPARE(result.get(), "/");
    }

    void testRealpathEmptyFilename()
    {
        SshCharPtr result(sftp_canonicalize_path(m_sftpSession.get(), ""));
        QVERIFY2(result != nullptr, getSftpErrorString(m_sftpSession.get()));
        QCOMPARE(result.get(), "/home");
    }

    void testRealpathResolve()
    {
        SshCharPtr result(sftp_canonicalize_path(m_sftpSession.get(), "/home/../tmp/././../home/../etc/./././"));
        QVERIFY2(result != nullptr, getSftpErrorString(m_sftpSession.get()));
        QCOMPARE(result.get(), "/etc");
    }

    void testRealpathRemoveSlash()
    {
        SshCharPtr result(sftp_canonicalize_path(m_sftpSession.get(), "/home/"));
        QVERIFY2(result != nullptr, getSftpErrorString(m_sftpSession.get()));
        QCOMPARE(result.get(), "/home");
    }

    void testRealpathResolveDirSymlink()
    {
        QTemporaryDir tempDir;
        tempDir.setAutoRemove(false);
        QVERIFY(tempDir.isValid());

        QVERIFY(QFile::link("/etc", tempDir.filePath("dir_symlink")));

        SshCharPtr result(sftp_canonicalize_path(
            m_sftpSession.get(), toChars(tempDir.filePath("dir_symlink/passwd"))));
        QVERIFY2(result != nullptr, getSftpErrorString(m_sftpSession.get()));
        QCOMPARE(result.get(), "/etc/passwd");
    }

    void testRealpathResolveFileSymlink()
    {
        QTemporaryDir tempDir;
        tempDir.setAutoRemove(false);
        QVERIFY(tempDir.isValid());

        QVERIFY(QFile::link("/dev/null", tempDir.filePath("file_symlink")));

        SshCharPtr result(sftp_canonicalize_path(
            m_sftpSession.get(), toChars(tempDir.filePath("file_symlink"))));
        QVERIFY2(result != nullptr, getSftpErrorString(m_sftpSession.get()));
        QCOMPARE(result.get(), "/dev/null");
    }

    //
    // READDIR

    void testRealpathEmptyDir()
    {
        QTemporaryDir tempDir;
        tempDir.setAutoRemove(false);
        QVERIFY(tempDir.isValid());

        sftp_dir dir = sftp_opendir(m_sftpSession.get(), toChars(tempDir.path()));
        QVERIFY2(dir != nullptr, getSftpErrorString(m_sftpSession.get()));

        sftp_attributes attrs = sftp_readdir(m_sftpSession.get(), dir);
        QVERIFY2(attrs != nullptr, getSftpErrorString(m_sftpSession.get()));
        QCOMPARE(attrs->name, ".");
        sftp_attributes_free(attrs);

        attrs = sftp_readdir(m_sftpSession.get(), dir);
        QVERIFY2(attrs != nullptr, getSftpErrorString(m_sftpSession.get()));
        QCOMPARE(attrs->name, "..");
        sftp_attributes_free(attrs);

        attrs = sftp_readdir(m_sftpSession.get(), dir);
        QVERIFY(attrs == nullptr);
        QCOMPARE(sftp_get_error(m_sftpSession.get()), SSH_FX_EOF);
        sftp_attributes_free(attrs);

        QVERIFY2(sftp_closedir(dir) == SSH_NO_ERROR, getSftpErrorString(m_sftpSession.get()));
    }

private:
    QString m_sshfsd;
    QString m_keyPath;
    QProcess m_sshfsdProcess;
    SshSessionPtr m_sshSession;
    SftpSessionPtr m_sftpSession;

    void startSshfsd();
};

//
// INIT

void TestSftpServer::initTestCase()
{
    m_keyPath = QStringLiteral("./ssh_server_key.rsa");
    m_sshfsd = qgetenv("SSHFSD");
    QVERIFY(!m_sshfsd.isEmpty());

    QVERIFY(generateRsaKey(m_keyPath));

    // start sshfsd
    m_sshfsdProcess.setReadChannelMode(QProcess::ForwardedChannels);
    m_sshfsdProcess.start(m_sshfsd, { "-p", "2222", "-r", m_keyPath, "localhost" });
    QVERIFY(m_sshfsdProcess.waitForStarted(1000));
    QVERIFY(m_sshfsdProcess.state() == QProcess::Running);

    QTest::qWait(100);

    // create ssh session
    m_sshSession.reset(ssh_new());
    QVERIFY(m_sshSession != nullptr);

    int verbosity = SSH_LOG_INFO;
    int port = 2222;
    ssh_options_set(m_sshSession.get(), SSH_OPTIONS_HOST, "localhost");
    ssh_options_set(m_sshSession.get(), SSH_OPTIONS_LOG_VERBOSITY, &verbosity);
    ssh_options_set(m_sshSession.get(), SSH_OPTIONS_PORT, &port);

    // connect
    QVERIFY2(
        ssh_connect(m_sshSession.get()) == SSH_OK,
        ssh_get_error(m_sshSession.get()));

    // auth
    QVERIFY2(
        ssh_userauth_password(m_sshSession.get(), "myuser", "mypassword") == SSH_OK,
        ssh_get_error(m_sshSession.get()));

    // sftp
    m_sftpSession.reset(sftp_new(m_sshSession.get()));
    QVERIFY(m_sftpSession != nullptr);
    QVERIFY2(
        sftp_init(m_sftpSession.get()) == SSH_OK,
        getSftpErrorString(m_sftpSession.get()));
}

bool generateRsaKey(const QString& filePath)
{
    if (QFile::exists(filePath) && !QFile::remove(filePath)) {
        qCritical() << "cannot remove rsa key" << filePath;
        return false;
    }

    QProcess process;
    process.setReadChannelMode(QProcess::ForwardedChannels);
    process.start("ssh-keygen", { "-q", "-t", "rsa", "-b", "4096", "-C", "test", "-f", filePath });
    if (!process.waitForFinished(5000)) {
        qCritical() << "ssh-keygen: timeout";
        return false;
    }

    return process.exitStatus() == QProcess::NormalExit && process.exitCode() == 0;
}

//
// CLEANUP

void TestSftpServer::cleanupTestCase()
{
    m_sshfsdProcess.terminate();
    QTRY_VERIFY_WITH_TIMEOUT(m_sshfsdProcess.state() == QProcess::NotRunning, 1000);
}

QTEST_MAIN(TestSftpServer)

#include "command_tests.moc"