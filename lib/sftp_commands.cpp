#include "sftp_commands.h"

#include <QDateTime>
#include <QDebug>
#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QString>
#include <algorithm>
#include <memory>

namespace {

int SSH_FX_NOT_A_DIRECTORY = 19;
int SSH_FX_INVALID_FILENAME = 20;

int READ_DIR_CHUNK_SIZE = 100;

template <typename T>
T* findHandle(sftp_client_message msg, const std::unordered_map<void*, std::unique_ptr<T>>& handles)
{
    void* handle = sftp_handle(msg->sftp, msg->handle);
    auto iter = handles.find(handle);
    if (iter == handles.end())
        return nullptr;
    return iter->second.get();
}

int toUnixPermissions(QFile::Permissions perms)
{
    return (perms & 0xF) | ((perms & 0xF0) >> 4) | ((perms & 0xF00) >> 8);
}

QString createLongName(const QFileInfo& file_info, const QString& filename)
{
    QString out;
    auto mode = file_info.permissions();

    if (file_info.isSymLink())
        out.append(QChar('l'));
    else if (file_info.isDir())
        out.append(QChar('d'));
    else
        out.append(QChar('-'));

    /* user */
    if (mode & QFileDevice::ReadOwner)
        out.append(QChar('r'));
    else
        out.append(QChar('-'));

    if (mode & QFileDevice::WriteOwner)
        out.append(QChar('w'));
    else
        out.append(QChar('-'));

    if (mode & QFileDevice::ExeOwner)
        out.append(QChar('x'));
    else
        out.append(QChar('-'));

    /*group*/
    if (mode & QFileDevice::ReadGroup)
        out.append(QChar('r'));
    else
        out.append(QChar('-'));

    if (mode & QFileDevice::WriteGroup)
        out.append(QChar('w'));
    else
        out.append(QChar('-'));

    if (mode & QFileDevice::ExeGroup)
        out.append(QChar('x'));
    else
        out.append(QChar('-'));

    /* other */
    if (mode & QFileDevice::ReadOther)
        out.append(QChar('r'));
    else
        out.append(QChar('-'));

    if (mode & QFileDevice::WriteOther)
        out.append(QChar('w'));
    else
        out.append(QChar('-'));

    if (mode & QFileDevice::ExeOther)
        out.append(QChar('x'));
    else
        out.append(QChar('-'));

    out.append(QString(" 1 %1 %2 %3").arg(file_info.ownerId()).arg(file_info.groupId()).arg(file_info.size()));
    out.append(QChar(' '));
    out.append(file_info.lastModified().toString("MMM d hh:mm:ss yyyy"));
    out.append(QChar(' '));
    out.append(filename);
    return out;
}

} // namespace

SftpServerSession::SftpServerSession(sftp_session sftp)
    : m_sftpSession(sftp)
    , m_cwd("/home")
{
}

SftpServerSession::~SftpServerSession() = default;

void SftpServerSession::run()
{
    int res = sftp_server_init(m_sftpSession.get());
    if (res < 0) {
        printf("Error sftp_server_init\n");
        return;
    }

    sftp_client_message msg;
    while (true) {
        msg = sftp_get_client_message(m_sftpSession.get());
        if (msg == nullptr) {
            printf("Error sftp_get_client_message\n");
            return;
        }
        int type = sftp_client_message_get_type(msg);
        switch (type) {
        case SSH_FXP_READLINK:
            replyReadLink(msg);
            break;
        case SSH_FXP_REALPATH:
            replyRealPath(msg);
            break;
        case SSH_FXP_OPENDIR:
            replyOpenDir(msg);
            break;
        case SSH_FXP_READDIR:
            replyReadDir(msg);
            break;
        case SSH_FXP_CLOSE:
            replyClose(msg);
            break;
        case SSH_FXP_OPEN:
            replyOpenFile(msg);
            break;
        case SSH_FXP_STAT:
            replyStat(msg, false);
            break;
        case SSH_FXP_LSTAT:
            replyStat(msg, true);
            break;
        case SSH_FXP_READ:
            replyReadFile(msg);
            break;
        default:
            replyUnsupported(msg);
            break;
        }
        sftp_client_message_free(msg);
    }
}

QString SftpServerSession::getFilename(sftp_client_message msg)
{
    QString filename = QString::fromUtf8(sftp_client_message_get_filename(msg));
    if (filename.isEmpty()) {
        return QString();
    }

    QFileInfo fileInfo{ filename };
    if (fileInfo.isRelative()) {
        return m_cwd.absoluteFilePath(filename);
    }

    return filename;
}

sftp_attributes_struct SftpServerSession::getFileAttrs(const QFileInfo& fileInfo)
{
    sftp_attributes_struct attr{};

    attr.size = fileInfo.size();

    attr.uid = fileInfo.ownerId();
    attr.gid = fileInfo.groupId();

    attr.permissions = toUnixPermissions(fileInfo.permissions());
    attr.atime = fileInfo.lastRead().toUTC().toMSecsSinceEpoch() / 1000;
    attr.mtime = fileInfo.lastModified().toUTC().toMSecsSinceEpoch() / 1000;
    attr.flags = SSH_FILEXFER_ATTR_SIZE
        | SSH_FILEXFER_ATTR_UIDGID
        | SSH_FILEXFER_ATTR_PERMISSIONS
        | SSH_FILEXFER_ATTR_ACMODTIME;

    if (fileInfo.isSymLink())
        attr.permissions |= SSH_S_IFLNK | 0777;
    else if (fileInfo.isDir())
        attr.permissions |= SSH_S_IFDIR;
    else if (fileInfo.isFile())
        attr.permissions |= SSH_S_IFREG;

    return attr;
}

bool SftpServerSession::replyUnsupported(sftp_client_message msg)
{
    int type = sftp_client_message_get_type(msg);
    qDebug() << "SFTP: UNSUPPORTED" << type;
    return sftp_reply_status(msg, SSH_FX_OP_UNSUPPORTED, "Unsupported message");
}

bool SftpServerSession::replyOk(sftp_client_message msg)
{
    return sftp_reply_status(msg, SSH_FX_OK, nullptr);
}

bool SftpServerSession::replyReadLink(sftp_client_message msg)
{
    auto filename = getFilename(msg);
    qDebug() << "SFTP: READLINK" << filename;

    if (filename.isEmpty()) {
        return sftp_reply_status(msg, SSH_FX_INVALID_FILENAME, "empty filename");
    }

    auto link = QFile::symLinkTarget(filename);
    if (link.isEmpty())
        return sftp_reply_status(msg, SSH_FX_NO_SUCH_FILE, "invalid link");

    sftp_attributes_struct attr{};
    sftp_reply_names_add(msg, link.toUtf8().data(), link.toUtf8().data(), &attr);
    return sftp_reply_names(msg) == 0;
}

bool SftpServerSession::replyRealPath(sftp_client_message msg)
{
    auto filename = getFilename(msg);
    // TODO: optional args: control-byte, compose-path

    qDebug() << "SFTP: REALPATH" << filename;
    if (filename.isEmpty()) {
        filename = m_cwd.path();
    }

    QString realPath = QFileInfo(filename).canonicalFilePath();
    if (realPath.isEmpty()) {
        return sftp_reply_status(msg, SSH_FX_NO_SUCH_FILE, "invalid file");
    }

    sftp_attributes_struct attr{};
    sftp_reply_names_add(msg, realPath.toUtf8().data(), realPath.toUtf8().data(), &attr);
    return sftp_reply_names(msg) == 0;
}

bool SftpServerSession::replyStat(sftp_client_message msg, bool follow)
{
    auto filename = getFilename(msg);

    QFileInfo fileInfo(filename);
    if (!fileInfo.isSymLink() && !fileInfo.exists())
        return sftp_reply_status(msg, SSH_FX_NO_SUCH_FILE, "no such file");

    sftp_attributes_struct attr = {};
    if (!follow && fileInfo.isSymLink()) {
        // TODO
        return sftp_reply_status(msg, SSH_FX_OP_UNSUPPORTED, "TODO: links");
    } else {
        if (fileInfo.isSymLink())
            fileInfo = QFileInfo(fileInfo.symLinkTarget());
        
        attr = getFileAttrs(fileInfo);
    }

    return sftp_reply_attr(msg, &attr);
}

bool SftpServerSession::replyOpenDir(sftp_client_message msg)
{
    auto filename = getFilename(msg);

    qDebug() << "SFTP: OPEN DIR" << filename;
    if (filename.isEmpty()) {
        return sftp_reply_status(msg, SSH_FX_INVALID_FILENAME, "empty filename");
    }

    QFileInfo fileInfo{ filename };
    if (!fileInfo.exists())
        return sftp_reply_status(msg, SSH_FX_NO_SUCH_FILE, "no such directory");
    if (!fileInfo.isDir())
        return sftp_reply_status(msg, SSH_FX_NOT_A_DIRECTORY, "no such directory");
    if (!fileInfo.isReadable())
        return sftp_reply_status(msg, SSH_FX_PERMISSION_DENIED, "permission denied");

    auto iterator_handle = std::unique_ptr<QDirIterator>(new QDirIterator(filename));
    void* handle = iterator_handle.get();
    m_openDirHandles.emplace(handle, std::move(iterator_handle));

    SshStringPtr sftp_handle{ sftp_handle_alloc(m_sftpSession.get(), handle) };
    qDebug() << "SFTP: NEW OPENDIR HANDLE" << filename << sftp_handle.get();
    return sftp_reply_handle(msg, sftp_handle.get());
}

bool SftpServerSession::replyReadDir(sftp_client_message msg)
{
    auto* iterator = findHandle(msg, m_openDirHandles);
    qDebug() << "SFTP: READ DIR" << iterator;

    if (!iterator)
        return sftp_reply_status(msg, SSH_FX_BAD_MESSAGE, "invalid handle");

    int i = 0;
    for (; i < READ_DIR_CHUNK_SIZE && iterator->hasNext(); ++i) {
        QFileInfo entry = iterator->next();
        auto attr = getFileAttrs(entry);
        QString longName = createLongName(entry, entry.fileName());

        QByteArray cFilename = entry.fileName().toUtf8();
        sftp_reply_names_add(msg, cFilename.data(), longName.toUtf8().data(), &attr);
        qDebug().noquote() << "SFTP: READ DIR: res +=" << longName;
    }

    if (i == 0) {
        qDebug() << "SFTP: READ DIR" << iterator << "EOF";
        return sftp_reply_status(msg, SSH_FX_EOF, nullptr);
    }

    return sftp_reply_names(msg);
}

bool SftpServerSession::replyClose(sftp_client_message msg)
{
    void* handle = sftp_handle(m_sftpSession.get(), msg->handle);
    qDebug() << "SFTP: CLOSE HANDLE" << handle;

    if (!m_openFileHandles.erase(handle) && !m_openDirHandles.erase(handle))
        return sftp_reply_status(msg, SSH_FX_BAD_MESSAGE, "invalid handle");

    sftp_handle_remove(m_sftpSession.get(), handle);
    return replyOk(msg);
}

bool SftpServerSession::replyOpenFile(sftp_client_message msg)
{
    auto filename = getFilename(msg);
    qDebug() << "SFTP: OPEN FILE" << filename;

    // flags
    QIODevice::OpenMode mode = QIODevice::Unbuffered;
    const auto flags = sftp_client_message_get_flags(msg);
    if (flags & SSH_FXF_READ)
        mode |= QIODevice::ReadOnly;
    if (flags & SSH_FXF_WRITE)
        mode |= QIODevice::WriteOnly;
    if (flags & SSH_FXF_APPEND)
        mode |= QIODevice::Append;
    if (flags & SSH_FXF_TRUNC)
        mode |= QIODevice::Truncate;

    // open
    auto file = std::make_unique<QFile>(filename);
    if (!file->open(mode)) {
        qDebug() << "SFTP: OPEN FILE ERROR" << filename << file->error() << file->errorString();
        return sftp_reply_status(msg, SSH_FX_FAILURE, file->errorString().toUtf8().data()); // TODO: convert file->error()
    }

    // TODO: set permissions
    void* handle = file.get();
    m_openFileHandles.emplace(handle, std::move(file));

    SshStringPtr sftp_handle{ sftp_handle_alloc(m_sftpSession.get(), handle) };
    return sftp_reply_handle(msg, sftp_handle.get());
}

bool SftpServerSession::replyReadFile(sftp_client_message msg)
{
    auto* file = findHandle(msg, m_openFileHandles);
    if (!file)
        return sftp_reply_status(msg, SSH_FX_BAD_MESSAGE, "invalid handle");

    size_t size = std::min(msg->len, 65536U);

    if (!file->seek(msg->offset)) {
        qDebug() << "SFTP: READ FILE ERROR" << file->fileName() << file->error() << file->errorString();
        return sftp_reply_status(msg, SSH_FX_FAILURE, file->errorString().toUtf8().data()); // TODO: convert file->error()
    }

    std::vector<char> buffer;
    buffer.reserve(size);
    auto bytes = file->read(buffer.data(), size);
    if (bytes < 0) {
        qDebug() << "SFTP: READ FILE ERROR" << file->fileName() << file->error() << file->errorString();
        return sftp_reply_status(msg, SSH_FX_FAILURE, file->errorString().toUtf8().data()); // TODO: convert file->error()
    }
    if (bytes == 0) {
        qDebug() << "SFTP: READ FILE" << file << msg->offset << msg->len << "EOF";
        return sftp_reply_status(msg, SSH_FX_EOF, nullptr);
    }

    qDebug() << "SFTP: READ FILE" << file << msg->offset << msg->len;
    return sftp_reply_data(msg, buffer.data(), bytes);
}

bool SftpServerSession::replyCloseFile(sftp_client_message msg)
{
    void* handle = sftp_handle(m_sftpSession.get(), msg->handle);
    qDebug() << "SFTP: CLOSE FILE" << handle;

    bool fileHandle = m_openFileHandles.erase(handle);
    if (!fileHandle)
        return sftp_reply_status(msg, SSH_FX_BAD_MESSAGE, "invalid handle");

    sftp_handle_remove(m_sftpSession.get(), handle);
    return replyOk(msg);
}
